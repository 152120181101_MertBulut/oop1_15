#pragma once
#ifndef ENGINE_H_
#define ENGINE_H_


class Engine
{
private:

		bool status;
		double fuel_per_second;
		double quantity;
		
public:

		bool start_engine();
		bool stop_engine();
		double absorb_fuel(double quantity);
		double give_back_fuel(double quantity);

};
#endif // !ENGINE_H_

