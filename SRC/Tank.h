#pragma once
#ifndef TANK_H_
#define TANK_H_


class Tank
{

private:
		
	int tank_id;
	double capasity;
	double fuel_quantity;
	bool broken;
	

public:

	int add_fuel_tank(double capacity);
	void list_fuel_tank();
	void remove_fuel_tank(int tank_id);
	bool connect_fuel_tank_to_engine(int tank_id);
	bool disconnect_fuel_tank_to_engine(int tank_id);
	bool open_valve(int tank_id);
	bool close_valve(int tank_id);
	int break_fuel_tank(int tank_id);
	int repair_fuel_tank(int tank_id);

};
#endif // !TANK_H_
