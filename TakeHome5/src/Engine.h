#pragma once
#include <iostream>
#include<stdlib.h>
#include<vector>
#include "Tank.h"

class Engine {

private:
	double fuel_per_second;
	bool status;
	Tank* tank;
	double internalTank = 55.0;
	
public:

	std::vector<Tank>tank_s;

	Engine(double, bool);
	void start_engine();
	void stop_engine();
	void add_fuel_tank(double);
	void list_fuel_tanks()const;
	void remove_fuel_tank(int);
	void break_fuel_tank(int);
	void repair_fuel_tank(int);
};