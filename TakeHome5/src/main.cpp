/**
* @file main.cpp
* @author MERT BULUT 152120181101<br>
* @author KISMET AKTAS 152120191088<br>
* @author ÖMER SENTURK 152120161084<br>
* @date 23.12.2020
*/

#include<iostream>
#include<fstream>
#include<string>
#include "File.h"

using namespace std;

int main(){

	FileInput input;
	fstream inputFile;
	inputFile.open("inputFile.txt", ios::in | ios::out);
	input.is_command(inputFile);
	
	return 0;
}