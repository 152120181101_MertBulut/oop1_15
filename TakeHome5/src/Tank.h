#pragma once
#include <iostream>
#include "Valve.h"
class Tank{

private:
	int id;
	double capacity;
	double fuel_quantity;
	bool broken;
	Valve *valve;
public:
	Tank(double, double, bool, int);
	void open_valve(int id);
	void close_valve(int id);
	//double absorb_fuel(double);
	double give_back_fuel(double);
};