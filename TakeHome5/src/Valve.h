#pragma once
#include <iostream>

class Valve{

private:
	int tankId;
public:
	Valve(int);
	void connect_fuel_tank_to_engine(int);
	void disconnect_fuel_tank_to_engine(int);
};
