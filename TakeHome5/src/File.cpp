#include<iostream>
#include<fstream>
#include<string>
#include "File.h"
#include "Engine.h"
#include "Tank.h"
#include "Valve.h"

using namespace std;

/**
* @brief:This function reads commands from a file.
* @param:fstream& commands-commands which are read from file.
*/
void FileInput::is_command(fstream& commands)
{
	string str;
	if (!commands.is_open())
	{
		cout << "File does not exist!" << endl;
		system("pause");
		exit(0);
	}
	if (commands.peek() == ifstream::traits_type::eof())
	{
		cout << "File is empty!" << endl;
		system("pause");
		exit(0);
	}
	while (commands >> str)
	{
		if (str == "start_engine;")
		{
		   /*Engine engine;
			engine.start_engine();*/
			cout << "Engine starts." /*<<engine.start_engine*/ << endl;
		}
		else if (str == "stop_engine;")
		{
		   /*Engine engine;
			engine.stop_engine();*/
			cout << "Engine stops." /*<<engine.stop_engine*/ << endl;
		}
		else if (str == "give_back_fuel")
		{
		   /*Tank tank;
			tank.give_back_fuel(100.0);*/
			cout << "Give back fuel:" /*<< tank.give_back_fuel*/ <<endl;
		}
		else if (str == "add_fuel_tank")
		{
		   /*Tank tank;
			tank.add_fuel_tank(100.0);*/
			cout << "Adding Tank :" /*<< tank.add_fuel_tank*/ <<endl;
			
		}
		else if (str == "list_fuel_tanks;")
		{
		   /*Engine engine;
			engine.list_fuel_tanks();*/
			cout << "Fuel Tanks List:" /*<< engine.list_fuel_tanks*/ << endl;
			
		}
		else if (str == "remove_fuel_tank")
		{
		   /*Engine engine;
			engine.remove_fuel_tank(1);*/
			cout << "Remove Fuel Tank:" /*<< engine.remove_fuel_tank*/ << endl;
		}
		else if (str == "connect_fuel_tank_to_engine")
		{
	       /*Valve valve;
			valve.connect_fuel_tank_to_engine(1);*/
			cout << "Connected fuel tank to engine!"/*<<valve.connect_fuel_tank_to_engine*/ << endl;
		}
		else if (str == "disconnect_fuel_tank_from_engine")
		{
		   /*Valve valve;
			valve.disconnect_fuel_tank_to_engine(1);*/
			cout << "Disconnected fuel tank to engine!" /*<<valve.disconnect_fuel_tank_to_engine*/ <<endl;
		}
		else if (str == "open_valve")
		{
		   /*Tank tank;
			tank.open_valve(1);*/
			cout << " Valve is open." /*<< tank.open_valve*/ <<endl;
		}
		else if (str == "close_valve")
		{
		   /*Tank tank;
			tank.close_valve(1);*/
			cout << " Valve is closed." /*<< tank.close_valve */ <<endl;
		}
		else if (str == "break_fuel_tank")
		{
		   /*Engine engine;
			engine.break_fuel_tank(1);*/
			cout << "Fuel tank breaked."/*<<engine.break_fuel_tank*/ << endl;
		}
		else if (str == "repair_fuel_tank")
		{
		   /*Engine engine;
			engine.repair_fuel_tank(1);*/
			cout << "Fuel tank repaired."/*<<engine.repair_fuel_tank*/ << endl;
		}
		else if (str == "stop_simulation;")
		{
			cout << "End of the simulation." << endl;
			exit(0);
		}
		else
		{
			cout << "This command does not exist!" << endl;
		}
	}
}
